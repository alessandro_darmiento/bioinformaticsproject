What is SAM: http://genome.sph.umich.edu/wiki/SAM
What is BAM: http://genome.sph.umich.edu/wiki/BAM
SAM example: https://www.biostars.org/p/932/
CIGAR, SAM parser lib: http://bioinformatics.cvr.ac.uk/blog/java-cigar-parser-for-sam-format/
PICARD, Java samtool implementation: http://broadinstitute.github.io/picard/
