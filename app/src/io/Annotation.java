package io;

import com.sun.istack.internal.Nullable;
import core.BioCore;
import core.GlobalVariables;
import io.gene.Gene;
import io.gene.Transcript;
import io.gene.sequence.Intron;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by alex on 29/09/16.
 */
public class Annotation {
    private final static String TAG = "app.src.io.annotation: ";
    private int aId; //Annotation unique id equals line
    private String strand=null;
    private String chr; //Chromosome name
    private String source; //Source program
    private String feature;
    private long startPoint;
    private long endPoint;
    private float score=-1; // '.' if not set
    private boolean forward=true; //+ = true, - = false
    private int frame = -1; //0, 1 or 2 or '.' if unset
    private JSONObject attributes; //couples key-value containing geneId, geneName, transcriptId and exonNum

    private boolean isBadValue; //Discard this annotation if this is true;

    /**
     * Create an Annotation from a GTF file line
     * @param line
     */
    public Annotation(String line, int aId){
        this.aId = aId;
        int j = 0;
        isBadValue=false;
        attributes = new JSONObject();
        //chr11	protein_coding	CDS	407817	407957	.	-	2	 gene_id "ENSG00000185187"; transcript_id "ENST00000332725"; exon_number "5"; gene_name "SIGIRR"; transcript_name "SIGIRR-201"; protein_id "ENSP00000333656";
        String[] splitLine = line.split("\t"); //This returns a list of 9 strings: first 8 are main values. Last one is the list of attributes
        /* for(int i = 0; i<splitLine.length; i++){
            System.out.println(TAG + "splitLine["+i+"]"+splitLine[i]);
        }*/

        //Read main values
        try{
            chr = splitLine[0];
            source = splitLine[1];
            feature = splitLine[2];
            startPoint = Long.valueOf(splitLine[3]);
            endPoint = Long.valueOf(splitLine[4]);
            if(!splitLine[5].contains("."))
            score = Float.valueOf(splitLine[5]); //-1 otherwise
            strand=splitLine[6];
            if(splitLine[6].contains("-"))
                forward=false; //true otherwise
            if(!splitLine[7].contains(".")) //-1 otherwise
                frame = Integer.valueOf(splitLine[7]);

        } catch (Exception e){
            System.out.println(TAG + "Error parsing main values: " + e.getMessage());
            isBadValue = true;
        }

        if(splitLine.length > 8){
            try{
                splitLine = splitLine[8].split(";"); //each string is a couple of key value with a space in between
            /*for(int i = 0; i<splitLine.length; i++){
                System.out.println(TAG + "splitLine["+i+"]"+splitLine[i]);
            }*/
                for(String s : splitLine){
                    String[] keyValue = s.split(" "); //Receive 3 strings, the first is a space and has to be discarded
                    if(keyValue.length>0) {
                        if (keyValue[0].length() < 2) {
                            attributes.put(keyValue[1], keyValue[2]);
                        } else {
                            attributes.put(keyValue[0], keyValue[1]);
                        }
                    }
                }
            } catch (Exception e){
                e.printStackTrace();
                System.out.println(TAG + "Error parsing attributes: " + e.getMessage());
                isBadValue = true;
            }
        }
    }

    /**
     * Create an Annotation to write a new GTF file
     */
    public Annotation(JSONObject params){
        Iterator<String> iterator = params.keys();
        isBadValue=false;
        try{
            /*
            while(iterator.hasNext()){
                String key = iterator.next();
                System.out.println(TAG + key + ": " + params.get(key));
            }
            */
            chr = params.getString(GlobalVariables.CHROMOSOME);
            source = GlobalVariables.APPLICATION_NAME;
            feature = params.getString(GlobalVariables.FEATURE);
            startPoint = params.getLong(GlobalVariables.START_POINT);
            endPoint = params.getLong(GlobalVariables.END_POINT);
            score = (float)params.getDouble(GlobalVariables.SCORE);
            forward = params.getBoolean(GlobalVariables.FORWARD);
            frame = params.getInt(GlobalVariables.FRAME);
            attributes = params.getJSONObject(GlobalVariables.ATTRIBUTES);
        } catch (Exception e){
            System.out.println(TAG + "Error creating main annotation fields: " + e.getMessage());
            e.printStackTrace();
            isBadValue=true;
        }
    }

    public String getChr() {
        return chr;
    }

    public String getFeature() {
        return feature;
    }

    public long getStartPoint() {
        return startPoint;
    }

    public long getEndPoint() {
        return endPoint;
    }

    public JSONObject getAttributes() {
        return attributes;
    }

    public boolean isBadValue(){
        return isBadValue;
    }

    /**
     * Recreate a GTF line
     * @return
     */
    @Override
    public String toString(){
    //chr11	protein_coding	CDS	407817	407957	.	-	2	 gene_id "ENSG00000185187"; transcript_id "ENST00000332725"; exon_number "5"; gene_name "SIGIRR"; transcript_name "SIGIRR-201"; protein_id "ENSP00000333656";
        Iterator<String> keyset = attributes.keys();
        String s = "";
        String key;
        s = s.concat(chr + "\t").concat(source + "\t").concat(feature + "\t").concat(startPoint + "\t").concat(endPoint + "\t");
        if(-1==score){
            s = s.concat(".\t");
        } else {
            s = s.concat(score + "\t");
        }
        if(true == forward){
            s = s.concat("+\t");
        } else {
            s = s.concat("-\t");
        }
        if(-1==frame){
            s = s.concat(".\t");
        } else {
            s = s.concat(frame + "\t");
        }


        while(keyset.hasNext()){key = keyset.next();
            try{
                s = s.concat(key + " ").concat(attributes.getString(key) + "; ");
            } catch (Exception e){
                System.out.println(TAG + "Error creating GTF string: " + e.getMessage());
                e.printStackTrace();
            }
        }
        s = s.concat("\n");
        return s;
    }

    /**
     * Create single Annotation obj from Intron and gene using virtual transcript
     * @return
     */
    public static Annotation makeAnnotation(Intron intron, Gene gene) {
        return makeAnnotation(intron, gene, null);
    }

    /**
     * Create single Annotation obj from Intron, gene and transcript
     * @return
     */
    public static Annotation makeAnnotation(Intron intron, Gene gene, @Nullable Transcript transcript) {
        JSONObject params = new JSONObject();
        JSONObject attributes = new JSONObject();
        try {
            params.put(GlobalVariables.CHROMOSOME, gene.getOwningChromosome());
            params.put(GlobalVariables.FEATURE, "intron"); //TODO: what feature is this?
            params.put(GlobalVariables.START_POINT, intron.getStartPosition());
            params.put(GlobalVariables.END_POINT, intron.getEndPosition());
            params.put(GlobalVariables.SCORE, "-1"); //TODO
            params.put(GlobalVariables.FORWARD, "true"); //TODO
            params.put(GlobalVariables.FRAME, "0"); //TODO
            attributes.put(GlobalVariables.GENE_ID, gene.getGeneId());
            attributes.put(GlobalVariables.INTRON_NUM, intron.getIntronNum());
            attributes.put(GlobalVariables.GENE_NAME, gene.getGeneName());
            if (null != transcript) {
                attributes.put(GlobalVariables.TRANSCRIPT_ID, transcript.gettId());
                attributes.put(GlobalVariables.TRANSCRIPT_NAME, transcript.getName());
            }
            params.put(GlobalVariables.ATTRIBUTES, attributes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Annotation(params);
    }

    /**
     * Create Annotation Array from gene using virtual transcript
     * @return
     */
    public static Annotation[] makeGeneAnnotations(Gene gene) {
        Annotation[] annotations = new Annotation[gene.getGeneIntrons().size()];
        int i = 0;
        for (Intron intron : gene.getGeneIntrons()) {
            annotations[i] = makeAnnotation(intron, gene);
            i++;
        }
        return annotations;
    }

    /**
     * Create Annotation Array from gene and transcript
     * @return
     */
    public static Annotation[] makeTrancriptAnnotations(Gene gene, Transcript transcript) {
        Annotation[] annotations = new Annotation[transcript.getIntrons().size()];
        int i = 0;
        for (Intron intron : transcript.getIntrons()) {
            annotations[i] = makeAnnotation(intron, gene, transcript);
            i++;
        }
        return annotations;
    }

}
