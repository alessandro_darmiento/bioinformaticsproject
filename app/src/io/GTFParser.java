package io;

import GUI.IRF;
import com.sun.istack.internal.Nullable;
import core.BioCore;
import core.GlobalVariables;
import io.gene.Gene;
import io.gene.Transcript;
import java.io.*;

/**
 * Created by alex on 29/09/16.
 */
public class GTFParser {
    private final static String TAG = "app.src.io.gtfparser: ";
    private String geneToSearch;
    private static File fOut;
    private static FileOutputStream fOutStream;
    private static FileOutputStream tempOutStream;
    private static BufferedWriter writer;
    private String fileName;
    private Gene currentGene = null;

    private int inputLines;
    private int outputLines;

    /**
     * @param fileName is the input file containing exons references
     */
    public GTFParser(String fileName, String outputFileName, @Nullable String geneToSearch) {
        this.fileName = fileName;
        this.geneToSearch = geneToSearch;
        if (!BioCore.getInstance().checkFiles(new String[]{fileName})) {
            System.out.print(TAG + "err");
            return;
        }


        inputLines = 0;
        outputLines = 0;
        try {
            System.out.println("Trying creating file: " + outputFileName);
            fOut = new File(outputFileName);
            if (!fOut.exists()) {
                if (fOut.createNewFile()) {
                    System.out.println("File Created");
                } else {
                    System.out.println("Cannot create output file");

                }
            }
            fOutStream = new FileOutputStream(fOut);
            writer = new BufferedWriter(new OutputStreamWriter(fOutStream));
        } catch (Exception e) {
            System.out.println(TAG + "Error cannot open output file: " + outputFileName + " e: " + e.getMessage());
            e.printStackTrace();
            IRF.getInstance().addLogMessage("Error cannot open output file: " + e.getMessage(), true);
        }
    }

    /**
     * Create genes from GTF annotation file.
     * A Gene obj is made by several Transcript obj and a list of exons called geneExons. geneExons represents a virual transcript
     * Each Transcript obj contains its own list of exons called transcriptExons
     * geneExons is computed by merging exons from all transcripts
     *
     * @return true if the process ended without errors, or false otherwhise
     */
    public boolean parse() {
        long time0 = System.currentTimeMillis(), time1, time2;
        String line;
        String transcriptId = "", transcriptName = "", geneId = "", geneName = "", chromosome = "", exonNum = "";
        Annotation annotation;
        int reportThreshold = 1;

        System.out.println(TAG + "parse is called");
        IRF.getInstance().addLogMessage("Computing introns...", false);

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            inputLines = 0;
            outputLines = 0;
            while ((line = br.readLine()) != null) {
                //chr11	protein_coding	CDS	407817	407957	.	-	2	 gene_id "ENSG00000185187"; transcript_id "ENST00000332725"; exon_number "5"; gene_name "SIGIRR"; transcript_name "SIGIRR-201"; protein_id "ENSP00000333656";
                annotation = new Annotation(line, inputLines);
                if (shouldAcceptAnnotation(annotation)) {
                    //System.out.println("Annotation " +  annotation.getAId() + " kept");
                    try {
                        chromosome = annotation.getChr();
                        geneId = annotation.getAttributes().getString(GlobalVariables.GENE_ID);
                        geneName = annotation.getAttributes().getString(GlobalVariables.GENE_NAME);
                        transcriptId = annotation.getAttributes().getString(GlobalVariables.TRANSCRIPT_ID);
                        transcriptName = annotation.getAttributes().getString(GlobalVariables.TRANSCRIPT_NAME);
                        exonNum = annotation.getAttributes().getString(GlobalVariables.EXON_NUM);
                    } catch (Exception e) {
                        System.out.println(TAG + "Error while getting tmp values");
                        e.printStackTrace();
                    }

                    if (null == currentGene) {
                        currentGene = new Gene(geneId, geneName, chromosome);
                    }

                    if (!geneId.equals(currentGene.getGeneId())) {
                        BioCore.getInstance().geneIsReady(currentGene, false);
                        currentGene = new Gene(geneId, geneName, chromosome);
                    }
                    currentGene.update(annotation);

                    //Add new annotation to gene
                    if (!currentGene.getTranscripts().containsKey(transcriptId)) {
                        //This annotation belongs to an new transcript
                        currentGene.getTranscripts().put(transcriptId, new Transcript(transcriptId, transcriptName));
                    }
                    //Insert exon values in trancscript (for output GTF)
                    currentGene.updateGeneExons(annotation);
                    ///
                }

                inputLines++;
                if (inputLines == reportThreshold) { //Print a report at each power of two
                    //System.out.println(TAG + "parsed line: " + inputLines);
                    IRF.getInstance().addLogMessage("parsed line: " + inputLines, false);
                    reportThreshold = reportThreshold * 2;
                }
            }
            //For last gene
            BioCore.getInstance().geneIsReady(currentGene, true);
            writer.close();
        } catch (Exception e) {
            System.out.println(TAG + "Error reading from input file: " + e.getMessage());
            e.printStackTrace();
            IRF.getInstance().addLogMessage("Error reading from input file: " + e.getMessage(), true);
            return false;
        }

        time1 = System.currentTimeMillis();

        System.out.println(TAG + "parse terminated");
        System.out.println(TAG + outputLines + " lines created from " + inputLines + " read. Output file weight is " + 100 * outputLines / inputLines + "% of original file");
        IRF.getInstance().addLogMessage("Process Executed in " + (time1 - time0) + " millis", false);

        if(!GlobalVariables.HIGH_SENSITIVITY){
            BioCore.getInstance().fastScanIntersect();
        }
        return true;
    }

    /**
     * Check parsing error, singleGeneMode and features filters
     * @param annotation
     * @return true if the annotation passed as argument is error free and refers to a feature not filtered.
     *         Is SingleGene mode is activated check also if the annotation refers to the interested gene.
     */
    private boolean shouldAcceptAnnotation(Annotation annotation) {
        //System.out.println("Checking annotation: " + annotation.getAId());
        if (annotation.isBadValue()) {//Always discard bad annotations
            //System.out.println("Annotation " +  annotation.getAId() + " isBadValue!!!");
            return false;
        }
        if (checkFilter(annotation.getFeature())) { //Discard annotation if feature belong to a filtered one
            //System.out.println("Annotation " +  annotation.getAId() + " feature is filtered: " + annotation.getFeature());
            return false;
        }
        if (null != geneToSearch) { //If singleGeneMode, discard all genes with different name or id
            try {
                if (annotation.getAttributes().has("gene_id") && annotation.getAttributes().getString("gene_id").equals(geneToSearch))
                    return true;
                if (annotation.getAttributes().has("gene_name") && annotation.getAttributes().getString("gene_name").equals(geneToSearch))
                    return true;
            } catch (Exception e) {
                //System.out.println(TAG + "Error while parsing + " + e);
                return false;
            }
            //System.out.println("Annotation " +  annotation.getAId() + " gene name or id doesn't match with geneToSearch!!!");
            return false; //Failed both search. Discard annotation
        }
        //System.out.println("Annotation " + annotation.getAId() + " passed all tests");
        return true;
    }

    /**
     * @param feature
     * @return true if this feature is inside filters array, false otherwise
     */
    private boolean checkFilter(String feature) {
        for (String s : GlobalVariables.FILTERS) {
            if (s.equals(feature))
                return true;
        }
        return false;
    }


    /**
     * Print an annotation object into output file
     *
     * @param annotation
     */
    public void printIntronsFile(Annotation annotation) {
        try {
            writer.write(annotation.toString());
            outputLines++;
        } catch (Exception e) {
            System.out.println(TAG + "Error printing on file: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * FIll a file with Gene info until a lines limit is reached
     * @return null if the threshold was not reaced, closed file otherwise
     */
    public File createOrUpdateTmpFile(Gene gene, boolean isLastGene){
        File tmp = new File(GlobalVariables.ABSOLUTE_OUTPUT_PATH() + GlobalVariables.TEMP_FILE_NAME + GlobalVariables.GTF_EXTENSION);
        try {
            if (!tmp.exists()) {
                tmp.createNewFile();
            } else { //Delete previous content
                if(GlobalVariables.INTERSECT_SEMAPHORE){
                    PrintWriter writer = new PrintWriter(tmp);
                    writer.print("");
                    writer.close();
                    GlobalVariables.INTERSECT_SEMAPHORE = false;
                    GlobalVariables.CURRENT_LINES = 0;
                }
            }

            tempOutStream = new FileOutputStream(tmp, true);
            for(Transcript t : gene.getTranscripts().values()){
                for(Annotation annotation : Annotation.makeTrancriptAnnotations(gene, t)){
                    addTmpLine(annotation);
                    GlobalVariables.CURRENT_LINES++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if((GlobalVariables.CURRENT_LINES > GlobalVariables.LINES_BUFFER_SIZE) || isLastGene){
            GlobalVariables.INTERSECT_SEMAPHORE = true;
            return tmp;
        }
        return null;
    }

    public void addTmpLine(Annotation annotation){
        String line = annotation.toString();
        try{
            tempOutStream.write(line.getBytes());
            tempOutStream.flush();
            //System.out.println(TAG + "Written line: " + line);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
