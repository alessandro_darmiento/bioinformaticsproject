package io.gene;

import core.GlobalVariables;
import io.Annotation;
import io.Retention;
import io.gene.sequence.Exon;
import io.gene.sequence.Intron;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

/**
 * Created by alex on 29/09/16.
 */
public class Gene {
    private final static String TAG = "app.src.io.gene.gene: ";
    private String owningChromosome;
    private String geneId;
    private String geneName;
    private TreeMap<String, Transcript> transcripts;
    private ArrayList<Exon> geneExons; //Merged exons from all transcripts of this gene
    private ArrayList<Intron> geneIntrons; //Merged introns from all transcripts of this gene
    private ArrayList retentions;

    public Gene(String geneId, String geneName, String owningChromosome){
        this.geneId = geneId;
        this.geneName = geneName;
        this.owningChromosome = owningChromosome;
        transcripts = new TreeMap<>();
        geneExons = new ArrayList<>();
        geneIntrons = new ArrayList<>();
    }

    public String getGeneId() {
        return geneId;
    }

    public String getGeneName() {
        return geneName;
    }

    public TreeMap<String, Transcript> getTranscripts() {
        return transcripts;
    }

    public String getOwningChromosome(){
        return owningChromosome;
    }

    public ArrayList<Exon> getGeneExons() {
        return geneExons;
    }

    /**
     * Debug stuff
     */
    public void printDebugStuff() {
        System.out.println("----");
        System.out.println("Printing information about gene: " + geneName);
        System.out.println("----");
        System.out.println("Printing exons: ");
        System.out.println("----");
        for (int i = 0; i < geneExons.size(); i++) {
            System.out.println("Exon: " + i);
            System.out.println("SeqId: " + geneExons.get(i).getExonNum());
            System.out.println("Start position: " + geneExons.get(i).getStartPosition());
            System.out.println("End position: " + geneExons.get(i).getEndPosition());
            System.out.println("----");
        }
        System.out.println("----");
        System.out.println("Printing introns: ");
        System.out.println("----");
        for (int i = 0; i < geneIntrons.size(); i++) {
            System.out.println("Intron: " + i);
            System.out.println("SeqId: " + geneIntrons.get(i).getIntronNum());
            System.out.println("Start position: " + geneIntrons.get(i).getStartPosition());
            System.out.println("End position: " + geneIntrons.get(i).getEndPosition());
            System.out.println("----");
        }

    }

    public void makeIntrons() {
        if(GlobalVariables.HIGH_SENSITIVITY){
            for(Transcript t : transcripts.values()){
                t.makeIntrons();
            }
        } else {
            Collections.sort(geneExons);
            for (int i = 1; i < geneExons.size(); i++) {
                geneIntrons.add(new Intron("Intron_" + i, geneExons.get(i - 1).getEndPosition() + 1, geneExons.get(i).getStartPosition() - 1));
            }
            checkOverlap();
        }
    }

    /**
     * Create geneExons from annotation
     *
     * @param annotation
     */
    public void updateGeneExons(Annotation annotation) {
        boolean isOverlapping = false;
        try {
            for (Exon e : geneExons) { //For each ExonGene I check if the annotation overlaps it
                if (e.isOverlapping(annotation.getStartPoint(), annotation.getEndPoint())) {
                    isOverlapping = true; //If so, update existing exonGenes
                    e.update(annotation);
                }
            }

            if (!isOverlapping) { //Otherwise add a new Exon to geneExons
                geneExons.add(new Exon(String.valueOf(geneExons.size() + 1), annotation.getStartPoint(), annotation.getEndPoint()));
            } else {
                //Exon structure is changed, check for overlaps in the whole exon array until no more overlaps exists
                checkOverlap();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check for overlapping regions in exons.
     * If an overlap is found, exons are merged
     */
    public void checkOverlap() {
        Collections.sort(geneExons);
        boolean isOverlapping = true; //Assuming at least one overlap
        try {
            while (isOverlapping) { //Repeat until not new overlaps are found
                isOverlapping = false;
                for (int i = 1; i < geneExons.size(); i++) {
                    if (geneExons.get(i - 1).isOverlapping(geneExons.get(i))) {
                        //System.out.println(TAG + "Found overlap!");
                        //System.out.println(TAG + "Exon " + (i - 1) + " starts at: " + geneExons.get(i - 1).getStartPosition() + " and end at: " + geneExons.get(i - 1).getEndPosition());
                        //System.out.println(TAG + "Exon " + (i) + " starts at: " + geneExons.get(i).getStartPosition() + " and end at: " + geneExons.get(i).getEndPosition());
                        merge(i - 1);
                        isOverlapping = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Merge two contiguos overlapping exons in one. Rename all exons according to new enumeration
     *
     * @param index is the index of the first gene
     */
    private void merge(int index) {
        //System.out.println(TAG + "Merging exon " + index + " with " + (index + 1));
        //System.out.println(TAG + "geneExons.size(): " + geneExons.size());
        try {
            Exon e1 = geneExons.get(index);
            Exon e2 = geneExons.get(index + 1);
            Exon e3 = new Exon(e1.getExonNum(), e1.getStartPosition(), e2.getEndPosition());
            for (int i = index + 2; i < geneExons.size(); i++) {
                geneExons.get(i).updateExonNum(String.valueOf(Long.valueOf(geneExons.get(i).getExonNum()) - 1));
            }
            geneExons.remove(index + 1);
            geneExons.remove(index);
            geneExons.add(e3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Intron> getGeneIntrons() {
        return geneIntrons;
    }

    public void update(Annotation annotation){
        if(GlobalVariables.HIGH_SENSITIVITY){ //Keep information about each transcript.
            try{
                String transcriptId = annotation.getAttributes().getString(GlobalVariables.TRANSCRIPT_ID);
                String transcriptName = annotation.getAttributes().getString(GlobalVariables.TRANSCRIPT_NAME);
                if (!getTranscripts().containsKey(transcriptId)) {
                    //This annotation belongs to an new transcript
                    getTranscripts().put(transcriptId, new Transcript(transcriptId, transcriptName));
                }
                transcripts.get(transcriptId).updateTranscriptExons(annotation);
            } catch (Exception e){
                e.printStackTrace();
            }

        } else { //Make a virtual transcript with all the annotation in the most conservative level
            updateGeneExons(annotation);
        }
    }

    public void addRetentions(ArrayList<Retention> retentions) {
        this.retentions = new ArrayList<>(retentions);
    }
    public ArrayList<Retention> getRetentions() {
        if(null == retentions){
            retentions = new ArrayList();
        }
       return retentions;
    }
}
