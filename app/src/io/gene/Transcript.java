package io.gene;

import core.GlobalVariables;
import io.Annotation;
import io.gene.sequence.Exon;
import io.gene.sequence.Intron;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by alex on 29/09/16.
 */
public class Transcript {
    private static final String TAG = "app.io.gene.transcript";
    ArrayList<Exon> transcriptExons;
    ArrayList<Intron> introns;
    private String tId;
    private String name;

    public Transcript(String tId, String name){
        this.tId = tId;
        this.name = name;
        transcriptExons = new ArrayList<>();
        introns = new ArrayList<>();
    }

    public String gettId() {
        return tId;
    }

    public String getName(){
        return name;
    }

    public ArrayList<Exon> getTranscriptExons() {
        return transcriptExons;
    }

    public ArrayList<Intron> getIntrons(){
        return introns;
    }

    public void makeIntrons(){
        Collections.sort(transcriptExons);
        for (int i = 1; i < transcriptExons.size(); i++) {
            introns.add(new Intron("Intron_" + i, transcriptExons.get(i - 1).getEndPosition() + 1, transcriptExons.get(i).getStartPosition() - 1));
        }
    }

    /**
     * Create geneExons from annotation
     *
     * @param annotation
     */
    public void updateTranscriptExons(Annotation annotation) {
        boolean exonExists = false;
        try {
            for (Exon e : transcriptExons) { //For each Exon I check if the annotation overlaps it
                if (e.isOverlapping(annotation.getStartPoint(), annotation.getEndPoint())) {
                    e.update(annotation);
                    exonExists = true;
                }
            }
            if(!exonExists){ //Add new exon
                transcriptExons.add(new Exon(String.valueOf(transcriptExons.size() + 1), annotation.getStartPoint(), annotation.getEndPoint()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
