package io.gene.sequence;

import io.Annotation;

/**
 * Created by alex on 29/09/16.
 */

public class Exon extends Sequence{

    public Exon(String exonNum){
        super(exonNum);
    }

    public Exon(String exonNum, long startPos, long endPos) {
        super(exonNum);
        setStartPosition(startPos);
        setEndPosition(endPos);
    }

    @Override
    public long getStartPosition() {
        return super.getStartPosition();
    }


    @Override
    public long getEndPosition() {
        return super.getEndPosition();
    }


    public String getExonNum() {
        return super.getsId();
    }

    /**
     * Grows exon boundaries
     * @param annotation
     */
    public void update(Annotation annotation){
        //if(annotation.isForward()){ //TODO what to do if it's backward?
            if(annotation.getStartPoint()< super.getStartPosition() || -1 == super.getStartPosition()){
                super.setStartPosition(annotation.getStartPoint());
            }
            if(annotation.getEndPoint()> super.getEndPosition() || -1 == super.getEndPosition()){
               super.setEndPosition(annotation.getEndPoint());
            }
        //} else {
            /* TODO: don't discard
            if(annotation.getEndPoint()<startPosition || -1 == startPosition){
                startPosition = annotation.getEndPoint();
            }
            if(annotation.getStartPoint()>endPosition || -1 == endPosition){
                endPosition = annotation.getStartPoint();
            }
            */
        //}
    }

    /**
     * Search for overlapping regions between this exon and the exon boundaries given in input
     *
     * @return true if overlapping, false otherwise
     */
    public boolean isOverlapping(long startPosition, long endPosition) {
        //if at leas one end is the same there is overlap
        if (this.getEndPosition() == endPosition || this.getStartPosition() == startPosition || this.getStartPosition() == endPosition || this.getEndPosition() == startPosition)
            return true;

        //otherwhise check
        if (this.getEndPosition() < startPosition)
            return false;
        if (this.getStartPosition() > endPosition)
            return false;
        return true;
    }

    public boolean isOverlapping(Exon exon) {
        return isOverlapping(exon.getStartPosition(), exon.getEndPosition());
    }

    public void updateExonNum(String exonNum) {
        super.updateSeqId(exonNum);
    }
}
