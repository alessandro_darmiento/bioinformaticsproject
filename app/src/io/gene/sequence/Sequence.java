package io.gene.sequence;

/**
 * Created by alex on 30/09/16.
 */
public class Sequence implements Comparable {

    private String sId;
    private long startPosition;
    private long endPosition;

    public Sequence(String sId){
        this.sId=sId;
        startPosition=-1;
        endPosition=-1;
    }

    public String getsId() {
        return sId;
    }

    public long getStartPosition() {
        return startPosition;
    }

    protected void setStartPosition(long startPosition) {
        this.startPosition = startPosition;
    }

    public long getEndPosition() {
        return endPosition;
    }

    protected void setEndPosition(long endPosition){
        this.endPosition = endPosition;
    }

    protected void updateSeqId(String seqId) {
        this.sId = seqId;
    }

    @Override
    public boolean equals(Object o) {
        if(this==o)
            return true;
        if((o==null) || (o.getClass() != this.getClass()))
            return false;
        // object must be Test at this point
        Sequence test=(Sequence) o;
        //two Seq are considered the same if they have the same sId
        if(null == test.getsId()) {
            return false;
        } else {
            return (test.getsId() == this.getsId());
        }
    }


    @Override //  http://javarevisited.blogspot.it/2011/11/how-to-override-compareto-method-in.html
    public int compareTo(Object o) {
        if (this.equals(o))
            return 0;
        //CompareTo method must return positive number if current object is less than other object, negative number if current object
        // is greater than other object and zero if both objects are equal to each other.
        Sequence current=this;
        Sequence compare=(Sequence) o;
        if(current.getStartPosition()==compare.getStartPosition())
            return 0;
        if(current.getStartPosition()>compare.getStartPosition())
            return 1;
        return -1;
    }
}
