package io.gene.sequence;

/**
 * Created by alex on 30/09/16.
 */
public class Intron extends Sequence{

    public Intron(String intronNum, long startPosition, long endPosition){
        super(intronNum);
        super.setStartPosition(startPosition);
        super.setEndPosition(endPosition);
    }

    public String getIntronNum(){
        return super.getsId();
    }

    @Override
    public long getStartPosition() {
        return super.getStartPosition();
    }

    @Override
    public long getEndPosition() {
        return super.getEndPosition();
    }
}
