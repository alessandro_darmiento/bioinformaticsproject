package io;

import GUI.IRF;
import core.BioCore;
import core.GlobalVariables;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;

import java.io.*;

/**
 * Created by Alex on 08/11/2016.
 * From this class it is possible to create a parallel process which execute Unix bash command
 */
public class BashExec {
    public final static String TAG = "app.io.bashexec: ";

    private static final String BEDTOOLS = "bedtools";
    private static final String SAMTOOLS = "samtools";
    private static final String BT_COMMAND_INTERSECT = "intersect";
    private static final String ST_COMMAND_CONVERT = "view";
    private static final String NONAMECHECK_OPTION = " -nonamecheck ";
    private static final String SORTED_OPTION = " -sorted ";

    public static String runScript(String command) {
        String errorMessage = null;
        System.out.println(TAG + "runScript: " + command);
        IRF.getInstance().addLogMessage("Executing command: " + command, false);
        String sCommandString = command;
        int iExitValue = -1;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            CommandLine oCmdLine = CommandLine.parse(sCommandString);
            DefaultExecutor oDefaultExecutor = new DefaultExecutor();
            oDefaultExecutor.setExitValue(0);
            PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
            oDefaultExecutor.setStreamHandler(streamHandler);
            try {
                iExitValue = oDefaultExecutor.execute(oCmdLine);
            } catch (ExecuteException e) {
                System.err.println("Execution failed: " + e.getMessage());
                e.printStackTrace();
                errorMessage = e.getMessage();
            } catch (IOException e) {
                System.err.println("permission denied.");
                e.printStackTrace();
                errorMessage = e.getMessage();
            }
        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = e.getMessage();
        }
        if(errorMessage!=null){
            IRF.getInstance().addLogMessage(errorMessage+"\n", true);
        } else {
            IRF.getInstance().addLogMessage("Command executed without errors\n", false);
        }
        return outputStream.toString();
    }

    /**
     * Check if BedTools is installed on the PC
     *
     * @return true if it's present
     */
    public static boolean checkBedToools() {
        String command = "bedtools --version";
        if (runScript(command).contains("command not found"))
            return false;
        return true;
    }



    /**
     * Check for overlap
     * Doc: http://bedtools.readthedocs.io/en/latest/content/tools/intersect.html
     * Convert SAM to BAM if possible
     * return error if files don't exist
     */
    public static String findOverlap(String referencePath, String featurePath) {
        System.out.println(TAG + "findOverlap");

        if(!BioCore.getInstance().checkFiles(new String[]{referencePath, featurePath})){
            return "Error: files don't exist";
        }

        if(featurePath.toLowerCase().endsWith(".sam")){
            featurePath = fromSamToBam(featurePath);
            //Double check because new file is created
            if(!BioCore.getInstance().checkFiles(new String[]{featurePath})){
                return "Error: files don't exist";
            }
        }

        ProcessBuilder builder = new ProcessBuilder("sort", "-k1,1", "-k4,4n", referencePath);
        builder.redirectOutput(new File(referencePath+".sorted.gtf"));
        builder.redirectError(new File(referencePath+".sorted.gtf"));
        Process p = null;
        try {
            p = builder.start();
            int j = p.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        StringBuilder command = new StringBuilder();
        command.append(BEDTOOLS);
        command.append(" ");
        command.append(BT_COMMAND_INTERSECT);
        command.append(NONAMECHECK_OPTION);
        command.append(SORTED_OPTION);
        command.append(" -a ");
        command.append(referencePath+".sorted.gtf");
        command.append(" -b ");
        command.append(featurePath+"alignedsort.bed");
        String res = runScript(command.toString());
        try {
            PrintWriter file = new PrintWriter(GlobalVariables.ABSOLUTE_OUTPUT_PATH() + "outbedtools.bed", "UTF-8");
            file.write(res);
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //bedGraph();
        return res;
    }

    public static void sortInput(String featurePath) {

        //1: check if the sorted file already exists
        String sortedPath = featurePath+"alignedsort.bed";
        if (new File(sortedPath).exists()){
            IRF.getInstance().addLogMessage("Sorted feature file already exists. Skipping further sorting\n", false);
            return;
        }else{
            ProcessBuilder builder = null;
            Process p = null;
            if (!new File(featurePath+"aligned.bed").exists()){
                builder = new ProcessBuilder("bedtools", "bamtobed");
                builder.redirectInput(new File(featurePath));
                builder.redirectOutput(new File(featurePath+"aligned.bed"));
                builder.redirectError(new File(featurePath+"aligned.bed"));

                try {
                    //p = Runtime.getRuntime().exec(String.valueOf(command));

                    p = builder.start();
                    int i = p.waitFor();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            builder = new ProcessBuilder("sort", "-k1,1", "-k2,2n", featurePath+"aligned.bed");
            builder.redirectOutput(new File(featurePath+"alignedsort.bed"));
            builder.redirectError(new File(featurePath+"alignedsort.bed"));
            p = null;
            try {
                //p = Runtime.getRuntime().exec(String.valueOf(command));

                p = builder.start();
                int i = p.waitFor();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public static String fromSamToBam(String samFilePath) {
        System.out.println(TAG + "fromSamToBam: " + samFilePath);
        String outputPath = samFilePath.replace(".sam", ".bam");
        String head = runScript("head " + samFilePath);
        System.out.println(TAG + "head: " + head);
        StringBuilder command = new StringBuilder();
        command.append(SAMTOOLS);
        command.append(" ");
        command.append(ST_COMMAND_CONVERT);
        command.append(" ");
        if (head.contains("@")) {
            //Header exists
            //samtools view -bS test.sam > test.bam
            command.append("-bS");

        } else {
            //Header is missing
            //samtools view -bT reference.fa test.sam > test.bam
            command.append("-bT");
            //TODO: add reference.fa (WHat's this?)
        }
        command.append(" ");
        command.append(samFilePath);
        command.append(" > ");
        command.append(outputPath);
        runScript(command.toString());
        System.out.println(TAG + "created: " + outputPath);
        return outputPath;
    }
}
