package io;

import core.GlobalVariables;
import io.gene.Gene;
import io.gene.sequence.Exon;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by flavia on 15/03/17.
 */
public class Retention {
    private final static String TAG = "app.src.retention: ";

    private String geneId;
    private String geneName;
    private String referringIntron;
    private long coordsStart;
    private long coordsEnd;
    private List<String> transcripts = new ArrayList<>();
    private Boolean isIntron; // Compare the retention with the virtual transcript and tell if this retention belong to an intronic region or not (maybe a false positive)
    private String chr;
    private boolean isBadValue = false;
    private int reads;


    public Retention(Annotation annot, HashMap<String, Gene> genesBuffer){
        //System.out.println(TAG + "new Retention with " + annot.toString());
        if(!annot.isBadValue()){
            try {
                coordsStart = annot.getStartPoint();
                coordsEnd = annot.getEndPoint();
                geneId = annot.getAttributes().getString(GlobalVariables.GENE_ID);
                geneName = annot.getAttributes().getString(GlobalVariables.GENE_NAME);
                referringIntron = annot.getAttributes().getString(GlobalVariables.INTRON_NUM);
                if(annot.getAttributes().has(GlobalVariables.TRANSCRIPT_ID)){
                    transcripts.add(annot.getAttributes().get(GlobalVariables.TRANSCRIPT_ID)+";"+annot.getAttributes().get(GlobalVariables.TRANSCRIPT_NAME)+";"+annot.getAttributes().get(GlobalVariables.INTRON_NUM));
                }
                chr = annot.getChr();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(null != genesBuffer){
                isIntron = true;
                try{
                    for(Exon x : genesBuffer.get(geneId).getGeneExons()){
                        if( (coordsStart>x.getStartPosition() && coordsStart<x.getEndPosition()) ||
                                (coordsEnd>x.getStartPosition() && coordsEnd<x.getEndPosition()) ||
                                (coordsStart<x.getStartPosition() && coordsEnd>x.getEndPosition())){
                            isIntron = false;
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            } else {
                isIntron = null;
            }
        } else {
            isBadValue = true;
        }
        reads=1;
        //printDebugStuff();
    }

    public Retention(Annotation annotation){
       this(annotation, null);
    }

    public String getGeneName() {
        return geneName;
    }


    public long getCoordsStart() {
        return coordsStart;
    }


    public long getCoordsEnd() {
        return coordsEnd;
    }

    public String getReferringIntron(){
        return referringIntron;
    }

    public List<String> getTranscripts() {
        return transcripts;
    }

    public void setCoordsStart(long coordsStart) {
        this.coordsStart = coordsStart;
    }

    public void setCoordsEnd(long coordsEnd) {
        this.coordsEnd = coordsEnd;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(chr + " ");
        sb.append(GlobalVariables.APPLICATION_NAME+ " ");
        sb.append(GlobalVariables.GENE_ID + ": " + geneId + " ");
        sb.append(GlobalVariables.GENE_NAME + ": " + geneName + " ");
        sb.append(GlobalVariables.INTRON_NUM + ": " + referringIntron + " ");
        sb.append("Start: " + coordsStart + " ");
        sb.append("End: " + coordsEnd + " ");
        if(transcripts.size()>0){
            sb.append("Supporting transcript: ");
            for(String s : transcripts){
                sb.append(s + " ");
            }
            if(null != isIntron){
                sb.append("Intronic Region: " + isIntron);
            }
        }
        sb.append(" Supporting Reads: "+reads);
        sb.append("\n");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass().equals(Retention.class)){
            Retention other = (Retention)obj;
            if(!this.haveSameTranscriptSet(other))
                return false;
            //System.out.println(TAG + "equals: " + other.getCoordsStart() + " : " + this.getCoordsStart() + ", " + other.getCoordsEnd() + " : " + this.getCoordsEnd());
            if(other.getCoordsStart() == this.getCoordsStart() && other.getCoordsEnd() == this.getCoordsEnd())
                return true;
            return false;
        }
        return false;
    }

    public boolean isDegenerate(){
        //System.out.println("Checking if retention is degenerate: " + coordsStart + " : " + coordsEnd);
        if((coordsEnd - coordsStart) == 0)
            return true;
        return false;
    }

    public boolean overlap(Retention other){
        if(other.getCoordsStart() < this.coordsEnd && other.getCoordsStart() > this.coordsStart)
            return true;
        if(other.getCoordsEnd() < this.coordsEnd && other.getCoordsEnd() > this.coordsStart)
            return true;
        if(this.getCoordsStart() < other.getCoordsEnd() && this.getCoordsStart() > other.getCoordsStart())
            return true;
        if(this.getCoordsEnd() < other.getCoordsEnd() && this.getCoordsEnd() > other.getCoordsStart())
            return true;
        return false;
    }


    public boolean haveSameTranscriptSet(Retention other) {
        if(transcripts.size() == 0 && other.getTranscripts().size() == 0)
            return true;
        for(String s : transcripts){
            if(!other.transcripts.contains(s))
                return false;
        }
        return true;
    }

    public boolean isBadValue() {
        return isBadValue;
    }

    public void setBadValue(boolean badValue) {
        isBadValue = badValue;
    }

    public void addRead() {
        reads++;
    }

    public int getReads() {
        return reads;
    }

    public void addRead(int reads) {
        this.reads += reads;
    }
}
