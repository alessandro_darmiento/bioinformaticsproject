package GUI;

import core.BioCore;

import javax.swing.*;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by alex on 29/09/16.
 */
public class IRF implements ActionListener{
    public static final String TAG = "app.gui.irf: ";

    private JPanel myJPanel;
    private JLabel appName;
    private JScrollPane logPanel;
    private JTextPane log;
    StyledDocument doc = log.getStyledDocument();

    private JTextField gtfInputPath;
    private JButton gtfBrowseButton;
    final JFileChooser fc = new JFileChooser();
    private JCheckBox singleGeneModeCheckBox;
    private JTextField geneIDTextField;
    private JCheckBox graphEnableCheckBox;
    private JRadioButton geneRadioButton;
    private JRadioButton transcriptRadioButton;
    private JTextField samInputPath;
    private JButton samBrowseButton;
    private JButton STARTButton;//TODO: make this work
    ButtonGroup group;
    private static final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    private JTextArea textArea1;
    private static IRF mInstance;
    private JButton button;
    private BioCore bioCore;

    private Style style;

    private Thread executionThread;


    public IRF (){
        gtfBrowseButton.addActionListener(this);
        samBrowseButton.addActionListener(this);
        STARTButton.addActionListener(this);
        logPanel.setMinimumSize(new Dimension((int)Math.round(screenSize.height*0.9),
                (int)Math.round(screenSize.width*0.3)));

        logPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        log.setEditable(false);
        logPanel.setViewportView(log);
        style = log.addStyle("I'm a Style", null);
        group = new ButtonGroup();

        group.add(geneRadioButton);
        group.add(transcriptRadioButton);
        geneRadioButton.setSelected(true);
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here

    }

    public static synchronized IRF getInstance(){
        if(mInstance==null){
            mInstance = new IRF();
        }
        return mInstance;
    }

    public void addLogMessage(final String message, final boolean isError) {
        SwingUtilities.invokeLater(new Runnable() { //Log message thread
            public void run() {
                String m = message;
                if(!message.endsWith("\n")){
                    m = message + "\n";
                }
                try{
                    if(!isError) {
                        StyleConstants.setForeground(style, Color.black);
                        //log.setText(log.getText() + message + "\n");
                        doc.insertString(doc.getLength(), m ,style);
                    }else{
                        StyleConstants.setForeground(style, Color.red);
                        String err = new String("ERROR: ");
                        err= err.concat(m);
                        doc.insertString(doc.getLength(), err ,style);
                        //log.setText(log.getText().concat("\n"+err));
                    }
                    log.setCaretPosition(log.getText().length()); //Non va
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public void clearLog(){
        SwingUtilities.invokeLater(new Runnable() { //Log message thread
            public void run() {
                log.setText("");
            }
        });
    }

    public JPanel getMyJPanel() {
        return myJPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //Handle open button action.
        if (e.getSource() == gtfBrowseButton) {
            int returnVal = fc.showOpenDialog(myJPanel);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                if(file.getName().substring(file.getName().length()-4).equals(".gtf")) {
                    gtfInputPath.setText(file.getAbsolutePath());
                }else{

                }
            }
        }else if (e.getSource() == samBrowseButton) {
            int returnVal = fc.showOpenDialog(myJPanel);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                if(file.getName().substring(file.getName().length()-4).equals(".sam") ||
                        file.getName().substring(file.getName().length()-4).equals(".bam")) {
                    samInputPath.setText(file.getAbsolutePath());
                }else{

                }
            }
        }else if (e.getSource() == STARTButton) {
            if(samInputPath.getText().length()>0 &&
                    gtfInputPath.getText().length()>0){
                String geneToSearch;
                if(singleGeneModeCheckBox.isSelected()) {
                    geneToSearch = geneIDTextField.getText();
                }else{ //else is needed to run multiple times
                    geneToSearch = null;
                }
                executionThread = new Thread(new Runnable() { //Separate thread
                    @Override
                    public void run() {
                        System.out.println("Start bioCore in separate thread");
                        addLogMessage("Computation begin", false);
                        bioCore.startComputation(gtfInputPath.getText(), samInputPath.getText(), geneToSearch,
                                transcriptRadioButton.isSelected(), graphEnableCheckBox.isSelected());
                    }
                });
                executionThread.start();
            }
        }
        return;
    }



    public void setBioCore(BioCore bio) {
        this.bioCore =bio;
    }
}
