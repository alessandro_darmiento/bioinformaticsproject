package core;

/**
 * Created by alex on 29/09/16.
 */
public class GlobalVariables {
    //public final static String APPLICATION_NAME = "stupid_non_efficient_intron_retention_actually_probably_not_finder";
    public final static String APPLICATION_NAME = "IRFinder";


    //GTFParser
    public final static String INTRONS_EXTENSION = "_introns";
    public final static String GTF_EXTENSION = ".gtf";
    public final static String GENE_ID = "gene_id";
    public final static String GENE_NAME = "gene_name";
    public final static String TRANSCRIPT_ID = "transcript_id";
    public final static String TRANSCRIPT_NAME = "transcript_name";
    public final static String EXON_NUM = "exon_number";
    public final static String[] FILTERS = {"CDS"};

    //Annotation
    public final static String CHROMOSOME = "chr";
    public final static String FEATURE = "feature";
    public final static String START_POINT = "start_point";
    public final static String END_POINT = "end_point";
    public final static String SCORE = "score";
    public final static String FORWARD = "forward";
    public final static String FRAME = "frame";
    public final static String INTRON_NUM = "intron_number";
    public final static String ATTRIBUTES = "attributes";

    public static boolean HIGH_SENSITIVITY = false;
    public static void setHighSensitivity(boolean highSensitivity) {
        HIGH_SENSITIVITY = highSensitivity;
    }

    public static final int LINES_BUFFER_SIZE = 20000; //TODO: tweak this value
    public static int CURRENT_LINES = 0;
    public static boolean INTERSECT_SEMAPHORE = false;

    public static String FILES_PATH = "";
    public static final String RESULTS_FOLDER = "IRFresults/";
    public static final String GRAPH_FOLDER = "Graphs/";
    public static final String OUTPUT_FILE_NAME = "intron_retentions";
    public static final String TEMP_FILE_NAME = "tmp";

    public static boolean ENABLE_GRAPHS = false;

    public static String ABSOLUTE_OUTPUT_PATH(){
        return FILES_PATH + RESULTS_FOLDER;
    }
}
