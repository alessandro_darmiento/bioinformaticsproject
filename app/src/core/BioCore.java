package core;

import GUI.IRF;
import com.sun.istack.internal.Nullable;
import io.*;
import io.gene.Gene;
import io.gene.Transcript;
import io.gene.sequence.Intron;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.json.JSONException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Alex on 28/09/2016.
 * This singleton performs all core computations and stores data structures
 */

public class BioCore {
    private final static String TAG = "app.src.core.biocore: ";
    private static BioCore mInstance = null;
    private GTFParser parser;
    private boolean isVerbose = false;

    private HashMap<String, Gene> genesBuffer; //Used by deepscan. Key is gene id
    private HashMap<String, ArrayList<Retention>> retentionListMap; //Used by deepscan to keep retentions info

    private String referencePath, featurePath, outputPath, outputFolder;
    private String gtfPath;

    public static synchronized BioCore getInstance() {
        if (null == mInstance)
            mInstance = new BioCore();
        return mInstance;
    }

    private BioCore() {
        System.out.println(TAG + "core.BioCore singleton created!");
        isVerbose = false; //TODO
    }

    public void startComputation(String gtfPath, String bamPath, @Nullable String geneToSearch, boolean highSensitivityMode, boolean graphEnabled) {
        filesInit(gtfPath);
        GlobalVariables.ENABLE_GRAPHS = graphEnabled;
        featurePath = bamPath;
        this.gtfPath = gtfPath;
        retentionListMap = new HashMap<>();
        GlobalVariables.setHighSensitivity(highSensitivityMode);
        IRF.getInstance().clearLog();
        IRF.getInstance().addLogMessage("Sorting input files", false);
        BashExec.sortInput(bamPath);
        parser = new GTFParser(gtfPath, referencePath, geneToSearch);
        parser.parse();
    }

    /**
     * Init method which creates or clean output folder and files
     * @param gtfPath is the path of the first input file and the folder in wich the output folder will be created
     */
    private void filesInit(String gtfPath){
        File f = new File(gtfPath);
        File dir;
        GlobalVariables.FILES_PATH = f.getPath().replace(f.getName(), "");
        outputFolder = GlobalVariables.FILES_PATH + GlobalVariables.RESULTS_FOLDER;
        outputPath = outputFolder + GlobalVariables.OUTPUT_FILE_NAME;
        referencePath = f.getAbsolutePath().replace(f.getName(), "") + f.getName() + GlobalVariables.INTRONS_EXTENSION + GlobalVariables.GTF_EXTENSION;
        dir = new File(outputFolder);
        if(!dir.isDirectory()){
           dir.mkdir();
        } else {
            //Delete all files in the outputFolder
            try{
                for(File file: dir.listFiles())
                    if (!file.isDirectory())
                        file.delete();
            } catch(Exception e)
            {
                e.printStackTrace();
            }

        }
        dir = new File(outputFolder + GlobalVariables.GRAPH_FOLDER);
        if(!dir.isDirectory()){
            dir.mkdir();
        } else {
            try{
                for(File file: dir.listFiles())
                    if (!file.isDirectory())
                        file.delete();
            } catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        String[] filesToInitialize = new String[]{outputPath, referencePath};
        File file;
        for(String s: filesToInitialize){
            file = new File(s);
            try{
                if(file.exists()){
                    PrintWriter writer = new PrintWriter(file);
                    writer.print("");
                    writer.close();
                } else{
                    file.createNewFile();
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Receives a complete Gene Obj.
     * If the program is working in fast scan mode, a virtual transcript is created merging all the gene
     * transcripts in a conservative way (Max exon length), then the gene introns are computed from the
     * virtual transcipt. Introns are then converted into Annotations and printed into introns GTF file.
     * BedTools computation will be executed later for all the introns GTF file.
     *
     * If the program is working in deep scan mode, introns are computed for each transcript and the gene
     * is stored in a buffer. When the buffer is full an intermediate BedTools computation is performed
     * are sent to a collapsing method. Collapsed results are then printed on the output file.
     *
     * @param gene
     */
    public void geneIsReady(Gene gene, boolean isLastGene) {
        //System.out.println("GeneIsReady " + gene.getGeneName());
        if(gene!=null){
            try {
                gene.makeIntrons();
                if (GlobalVariables.HIGH_SENSITIVITY) {
                    deepScanComputation(gene, isLastGene);
                } else {
                    fastScanComputation(gene);
                }
                //Printing stuff
                if (BioCore.getInstance().isVerbose()) {
                    printGeneInfo(gene);
                    //gene.printDebugStuff();
                }
            } catch (Exception e) {
                System.out.println("Error parsing gene.");
                e.printStackTrace();
            }
        }
    }

    /**
     * Store introns data into GTF file and discard the gene
     * @param gene
     */
    private void fastScanComputation(Gene gene){
        for (Annotation annotation : Annotation.makeGeneAnnotations(gene)) {
            parser.printIntronsFile(annotation);
        }
    }

    public void fastScanIntersect(){
        long time0 = System.currentTimeMillis(), time1, time2, time3;
        ArrayList<Retention> retentions;
        IRF.getInstance().addLogMessage("Executing BEDTOOLS INTERSECT", false);
        String intersectRetsult = BioCore.getInstance().beginIntersect();
        time1 = System.currentTimeMillis();
        IRF.getInstance().addLogMessage("Approximately " + intersectRetsult.length()/150 + " raw intersect results received", false);
        IRF.getInstance().addLogMessage("Process Executed in " + (time1 - time0) + " millis", false);
        IRF.getInstance().addLogMessage("Execution post processing operations", false);
        retentions = collapseResults(intersectRetsult);
        time2 = System.currentTimeMillis();
        IRF.getInstance().addLogMessage(retentions.size() + " retentions discovered", false);
        IRF.getInstance().addLogMessage("Process Executed in " + (time2 - time1) + " millis", false);
        IRF.getInstance().addLogMessage("Writing output to file", false);
        makeOutputFile(retentions);
        if(GlobalVariables.ENABLE_GRAPHS){
            createGraph(makeDataSet(retentions));
            time3 = System.currentTimeMillis();
            IRF.getInstance().addLogMessage("Process Executed in " + (time3 - time2) + " millis", false);
            IRF.getInstance().addLogMessage("Computation Executed in " + (time3 - time0) + " millis", false);
        } else {
            IRF.getInstance().addLogMessage("Computation Executed in " + (time2 - time0) + " millis", false);
        }
    }


    public void createGraph(ArrayList<DefaultCategoryDataset> data){
        int i =0;
        for(DefaultCategoryDataset dataset : data) {
            i++;
            System.out.println(TAG + "making graph. Dataset size: " + dataset.getColumnCount()*dataset.getRowCount());
            JFreeChart barChart = ChartFactory.createBarChart3D(
                    "Intron Retention Statistics",
                    "Gene",
                    "Quantity",
                    dataset,
                    PlotOrientation.VERTICAL,
                    true, true, false);

            int width = 6400; /* Width of the image */
            int height = 4800; /* Height of the image */
            File barChart3D = new File(GlobalVariables.ABSOLUTE_OUTPUT_PATH() + GlobalVariables.GRAPH_FOLDER + "IRFGraph_"+i+".jpeg");
            try {
                ChartUtilities.saveChartAsJPEG(barChart3D, barChart, width, height);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<DefaultCategoryDataset> makeDataSet(HashMap<String, ArrayList<Retention>> retentionsMap){
        ArrayList<Retention> mergedRetentions = new ArrayList<>();
        for(String k : retentionsMap.keySet()){
             mergedRetentions.addAll(retentionsMap.get(k));
        }
        return makeDataSet(mergedRetentions);
    }

    public ArrayList<DefaultCategoryDataset> makeDataSet(ArrayList<Retention> retentions){
        ArrayList<DefaultCategoryDataset> list = new ArrayList<>();
        HashMap<String, HashMap<String, Integer>> stats = makeStats(retentions);
        int i = 0;
        DefaultCategoryDataset dataset = null;
        for(String gene : stats.keySet()){
            if(i==0){
                dataset = new DefaultCategoryDataset();
            }
            i++;
            for(String intron : stats.get(gene).keySet()){
                dataset.addValue(stats.get(gene).get(intron), gene, intron);
            }
            if(i==230){
                list.add(dataset);
                i=0;
            }
        }
        if(i!=0)
            list.add(dataset);
        return list;
    }

    private HashMap<String, HashMap<String, Integer>> makeStats(ArrayList<Retention> retentions) {
        HashMap<String, HashMap<String, Integer>> stats = new HashMap<>();
        for(Retention ret : retentions){
            if(stats.keySet().contains(ret.getGeneName())){
                if(stats.get(ret.getGeneName()).keySet().contains(ret.getReferringIntron())){
                    int val = stats.get(ret.getGeneName()).get(ret.getReferringIntron());
                    val++;
                    stats.get(ret.getGeneName()).put(ret.getReferringIntron(), val);
                }else{
                    stats.get(ret.getGeneName()).put(ret.getReferringIntron(), 1);
                }
            }else{
                HashMap<String, Integer> map = new HashMap<>();
                map.put(ret.getReferringIntron(), 1);
                stats.put(ret.getGeneName(), map);
            }
        }
        return stats;
    }


    /**
     * Store gene in genesBuffer and compute intermediate results when buffer is full
     * @param gene
     */
    private void deepScanComputation(Gene gene, boolean isLastGene){
        if (null == genesBuffer) {
            genesBuffer = new HashMap<>();
        }
        File f;
        String intersectResult;
        // TODO list
        // (1) Make BED for single gene transcrip.
        //File f = parser.createNewTmpFile(gene);
        genesBuffer.put(gene.getGeneId(), gene);
        f = parser.createOrUpdateTmpFile(gene, isLastGene);

        // (2) Perform intersect with given BAM
        if (null != f) {
            System.out.println("GenesBuffer size: " + genesBuffer.size());
            referencePath = f.getAbsolutePath();
            // (3) Collapse results and keep info about supporting transcripts
            intersectResult = beginIntersect();
            //System.out.println("Insersect results: \n" + intersectResult);
            collapseResults(intersectResult, genesBuffer);
            makeOutputFile(genesBuffer);
            genesBuffer.clear();
        }

    }

    private ArrayList<Retention> collapseResults(String intersectResult){
        return collapseResults(intersectResult, null);
    }

    /**
     * Collapse BedTools raw output into a list of intron retentions.
     * This method merge overlapping intron retention results from raw BedTools. If the program is ran in deep scan mode,
     * two retentions will be merged only if the supporting trancripts list is the same.
     * @param intersectResult
     * @param genesBuffer
     * @return
     */
    private ArrayList<Retention> collapseResults(String intersectResult, HashMap<String, Gene> genesBuffer) {

        String[] splitResult = intersectResult.split("\n"); //Split intersect result, which is an unique big string, into a string array
        intersectResult = null; //Make it garbage colletable
        ArrayList<Retention> retentions = new ArrayList<>(); //List of actual retentions
        String lastGene = new String();
        Retention retention, tmp;
        Annotation annot;
        boolean flag = false;

        for (int i = 0; i < splitResult.length && !splitResult.equals(""); i++) {
            annot = new Annotation(splitResult[i], i);
            if(!annot.isBadValue()){
                if(GlobalVariables.HIGH_SENSITIVITY){
                    try {
                        if (0 == i) {
                            lastGene = annot.getAttributes().getString(GlobalVariables.GENE_ID);
                        } else {
                            if (!lastGene.equals(annot.getAttributes().getString(GlobalVariables.GENE_ID))) {
                                genesBuffer.get(lastGene).addRetentions(retentions);
                                //retentionListMap.put(lastGene, retentions);
                                retentions.clear();
                                lastGene = annot.getAttributes().getString(GlobalVariables.GENE_ID);
                            }
                        }

                        flag = false;
                        for (Retention ret : retentions) {
                            tmp = new Retention(annot, genesBuffer);
                            if(!tmp.isBadValue()){
                                if (ret.equals(tmp)) { //check if they have same coordinates (do not check supporting transcript!)
                                    if(!ret.getTranscripts().contains(annot.getAttributes().get(GlobalVariables.TRANSCRIPT_ID) + ";" + annot.getAttributes().get(GlobalVariables.TRANSCRIPT_NAME) + ";" + annot.getAttributes().get(GlobalVariables.INTRON_NUM))) {
                                        ret.getTranscripts().add(annot.getAttributes().get(GlobalVariables.TRANSCRIPT_ID) + ";" + annot.getAttributes().get(GlobalVariables.TRANSCRIPT_NAME) + ";" + annot.getAttributes().get(GlobalVariables.INTRON_NUM));
                                    }
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        if(!flag){
                            retentions.add(new Retention(annot, genesBuffer));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try{
                        if(0 == i){
                            retentions.add(new Retention(annot));
                        } else {
                            retention = new Retention(annot);
                            if(!retention.isDegenerate()){ //If degenerate discard
                                if(!retention.equals(retentions.get(retentions.size()-1))){ //If equal discard
                                    if(retention.overlap(retentions.get(retentions.size()-1))){ //If overlap merge
                                        tmp = retentions.remove(retentions.size()-1);
                                        tmp.setCoordsStart(Math.min(tmp.getCoordsStart(), retention.getCoordsStart()));
                                        tmp.setCoordsEnd(Math.max(tmp.getCoordsEnd(), retention.getCoordsEnd()));
                                        tmp.addRead();
                                        retentions.add(tmp);
                                    } else { //If completely different, add as new one
                                        retentions.add(retention);
                                    }
                                }else{
                                    retentions.get(retentions.size()-1).addRead();
                                }
                            }
                        }
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
        //For last gene (or the only one if singleGene)
        if(GlobalVariables.HIGH_SENSITIVITY){ //Search for overlapping results which cannot be found during computation due to different supporting transcripts
            genesBuffer.get(lastGene).addRetentions(retentions);
            retentions.clear(); //TODO: chi se ne frega di sta roba?
            deepScanMergeFinalResult(genesBuffer);

        }
        return retentions;
    }

    /**
     * Due to different transcripts sets, retentions merging cannot be executed in parallel with adding them to the geneBuffer.
     * This method analyze all the retentions and merge them according to both overlaps and transcripts sets
     * @param genesBuffer
     */
    private void deepScanMergeFinalResult(HashMap<String, Gene> genesBuffer){
        ArrayList<Retention> aux;
        Retention inner, outer;

        for(String g : genesBuffer.keySet()){
            retentionListMap.put(g, genesBuffer.get(g).getRetentions());
            aux = new ArrayList<>();
            if(genesBuffer.get(g).getRetentions().size()>0){

                for(int i = 0; i< genesBuffer.get(g).getRetentions().size(); i++){
                    outer = genesBuffer.get(g).getRetentions().get(i);
                    if(!outer.isBadValue()){ //This flag is raised when a retention is found to overlap another one and therefore a single merged retention is used
                        for(int j = i+1; j < genesBuffer.get(g).getRetentions().size(); j++){
                            inner =  genesBuffer.get(g).getRetentions().get(j);
                            if(!inner.equals(outer)){ //If equals, discard inner
                                if(outer.overlap(inner) && outer.haveSameTranscriptSet(inner)){
                                    outer.setCoordsStart(Math.min(outer.getCoordsStart(), inner.getCoordsStart()));
                                    outer.setCoordsEnd(Math.max(outer.getCoordsEnd(), inner.getCoordsEnd()));
                                    outer.addRead(inner.getReads());
                                    inner.setBadValue(true); //Raise the flag to prevent using again the same retention
                                }
                            }else{
                                inner.setBadValue(true);
                                outer.addRead(inner.getReads());
                            }
                        }
                        aux.add(outer);
                    }
                }
                //Now replace retentions with the aux set
                genesBuffer.get(g).getRetentions().clear();
                genesBuffer.get(g).addRetentions(aux);
                retentionListMap.remove(g);
                retentionListMap.put(g, aux);
            }
        }
        deepScanShowResults();
    }

    private void deepScanShowResults(){
        long time0 = System.currentTimeMillis(), time1, time2;
        int total = 0;
        for(String s : retentionListMap.keySet()){
            total += retentionListMap.get(s).size();
        }
        IRF.getInstance().addLogMessage("Found "+ total + "retentions", false);
        IRF.getInstance().addLogMessage("Generating graph ", false);
        //Graph
        if(GlobalVariables.ENABLE_GRAPHS){
            createGraph(makeDataSet(retentionListMap));
            time1 = System.currentTimeMillis();
            IRF.getInstance().addLogMessage("Process exectuted in " + (time1 - time0) + " millis", false);
        }
        time2 = System.currentTimeMillis();
        IRF.getInstance().addLogMessage("Process exectuted in " + (time2 - time0) + " millis", false);


    }

    /**
     * Print parsed information about the gene in the GUI: exons, instrons, transcript....
     * @param gene
     */
    private void printGeneInfo(Gene gene) {
        //ArrayList<Exon> exons;
        IRF.getInstance().addLogMessage("Gene: " + gene.getGeneId() + ", " + gene.getGeneName() + " is ready", false);
        if (GlobalVariables.HIGH_SENSITIVITY) {
            IRF.getInstance().addLogMessage("For this gene " + gene.getTranscripts().size() + " different transcripts are found", false);
            for (Transcript t : gene.getTranscripts().values()) {
                IRF.getInstance().addLogMessage("   Transcript: " + t.gettId() + ", " + t.getName() + " has " + t.getTranscriptExons().size() + " exons", false);
                for (int i = 0; i < t.getTranscriptExons().size(); i++) {
                    IRF.getInstance().addLogMessage("       Exon: " + t.getTranscriptExons().get(i).getExonNum() + " starts from: " + t.getTranscriptExons().get(i).getStartPosition() + " and ends at: " + t.getTranscriptExons().get(i).getEndPosition(), false);
                    if (i > 0 && (t.getTranscriptExons().get(i - 1).getEndPosition() > t.getTranscriptExons().get(i).getStartPosition())) {
                        IRF.getInstance().addLogMessage("       FOUND OVERLAP BETWEEN EXONS " + t.getTranscriptExons().get(i - 1).getExonNum() + " and " + t.getTranscriptExons().get(i).getExonNum(), false);
                    }
                }
                IRF.getInstance().addLogMessage("   Transcript: " + t.gettId() + ", " + t.getName() + " has " + t.getIntrons().size() + " introns", false);
                for (Intron intron : t.getIntrons()) {
                    IRF.getInstance().addLogMessage("       Intron: " + intron.getIntronNum() + " starts from: " + intron.getStartPosition() + " and ends at: " + intron.getEndPosition(), false);
                }
            }
        } else {
            IRF.getInstance().addLogMessage("   Gene: " + gene.getGeneId() + ", " + gene.getGeneName() + " has " + gene.getGeneExons().size() + " exons", false);
            for (int i = 0; i < gene.getGeneExons().size(); i++) {
                IRF.getInstance().addLogMessage("       Exon: " + gene.getGeneExons().get(i).getExonNum() + " starts from: " + gene.getGeneExons().get(i).getStartPosition() + " and ends at: " + gene.getGeneExons().get(i).getEndPosition(), false);
                if (i > 0 && (gene.getGeneExons().get(i - 1).isOverlapping(gene.getGeneExons().get(i - 1)))) {
                    IRF.getInstance().addLogMessage("       FOUND OVERLAP BETWEEN EXONS " + gene.getGeneExons().get(i - 1).getExonNum() + " and " + gene.getGeneExons().get(i).getExonNum(), false);
                }
            }
            IRF.getInstance().addLogMessage("   Gene " + gene.getGeneId() + ", " + gene.getGeneName() + " has " + gene.getGeneIntrons().size() + " introns", false);
            for (Intron intron : gene.getGeneIntrons()) {
                IRF.getInstance().addLogMessage("       Intron: " + intron.getIntronNum() + " starts from: " + intron.getStartPosition() + " and ends at: " + intron.getEndPosition(), false);
            }
        }
    }

    public boolean isVerbose() {
        return isVerbose;
    }

    public boolean checkFiles(String[] fileList) {
        System.out.println(TAG + "checkFile");
        File file;
        for (String s : fileList) {
            System.out.println(TAG + "checking: " + s);
            file = new File(s);
            if (!file.exists()) {
                System.out.println(TAG + "file does not exist!!");
                return false;
            }
            System.out.println(TAG + "file exists");
        }
        return true;
    }

    /**
     * Use bedtools to intersect newly created GTF file with given SAM/BAM file
     * @return
     */
    public String beginIntersect() {
        try {
            String intersections = BashExec.findOverlap( referencePath, featurePath);
            //System.out.println(TAG + intersections);
            System.out.println(TAG + "Bedtools Intersect done");
            return intersections;
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    private void makeOutputFile(HashMap<String, Gene> genes) {
        BufferedWriter writer = makeOutputFile();
        try {
            for (Gene g : genes.values()) {
                if (null != g.getRetentions()) {
                    for (Retention retention : g.getRetentions()) {
                        //System.out.println(TAG + "printing: " + retention.toString());
                        writer.write(retention.toString());
                    }
                }
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }

    private void makeOutputFile(ArrayList<Retention> retentions){
        BufferedWriter writer = makeOutputFile();
        try{
            for(Retention retention : retentions){
                //System.out.println(TAG + "printing: " + retention.toString());
                writer.write(retention.toString());
            }
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return;
    }

    private BufferedWriter makeOutputFile(){
        File output = new File(outputPath);
        FileOutputStream fileOutputStream;
        BufferedWriter writer;
        if (!output.exists()) {
            try {
                output.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try{
            fileOutputStream = new FileOutputStream(output, true);
            writer = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
            return writer;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
