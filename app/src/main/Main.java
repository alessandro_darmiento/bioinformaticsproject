package main;


import GUI.IRF;
import core.BioCore;
import core.GlobalVariables;
import io.BashExec;

import javax.swing.*;
import java.io.IOException;


/**
 * Created by Alex on 28/09/2016.
 */
public class Main {
    private static final String TAG = "app.src.main.main: ";


    public static void main(String[] args){
        BioCore.getInstance();
        IRF.getInstance().setBioCore(BioCore.getInstance());
        //GUI
        SwingUtilities.invokeLater(new Runnable() { //Separate thread for GUI
            public void run() {
                JFrame frame = new JFrame("IRF");
                frame.setContentPane(IRF.getInstance().getMyJPanel());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setVisible(true);
            }
        });

 //TEST without gui
/*
        System.out.println(TAG + "===================================================================================");
        System.out.println(TAG + System.getProperty("os.name"));
        if(System.getProperty("os.name").startsWith("Windows")){
            System.out.println("ERR: system not supported! Abort");
            return;
        }
        System.out.println(TAG + "===================================================================================");



        if(!BashExec.checkBedToools()){
            System.out.println(TAG + "bedtools not found. Installing bedtools");
            BashExec.installBedTools();
        }

        if(!BashExec.checkSamToools()){
            System.out.println(TAG + "samtools not found. Installing samtools");
            BashExec.installSamTools();
        }

        BioCore.getInstance().launchBioCore(null, GlobalVariables.SMALLER_SAMPLE_FILE_LOCATION, GlobalVariables.BAM_SAMPLE,
                GlobalVariables.SMALLER_SAMPLE_FILE_LOCATION + "_PARSED", false,
                false, false, true);

        //String test = BashExec.findOverlap(GlobalVariables.BED_SAMPLE, GlobalVariables.BAM_SAMPLE);
        //System.out.println(TAG + test);

*/
    }

}
